const addButton = document.getElementById("add-button")
const calculateButton = document.getElementById("calculate-button")
const notaInput = document.getElementById("nota")
const notasList = document.getElementById("notas-list")
const result = document.getElementById("result")
const errorMessage = document.getElementById("error-message")
let notas = []

addButton.addEventListener("click", addNota);
calculateButton.addEventListener("click", calculateMedia)
var i = 1
function addNota() {
  const nota = notaInput.value
  if (!nota) {
    alert("Por favor, insira uma nota")
    return
  }
  if (isNaN(nota) || nota < 0 || nota > 10) {
    alert("A nota digitada é inválida, por favor, insira uma nota válida.")
    return
  }
  notas.push(nota);
  const item = document.createElement("li")
  item.textContent = `A nota ${i} foi: ` + nota
  i++
  notasList.appendChild(item)
  
  
}

function calculateMedia() {
  if (notas.length === 0) {
    displayError("Não há notas suficientes para calcular a média.")
    return;
  }
  let sum = 0
  for (let i = 0; i < notas.length; i++) {
    sum += parseFloat(notas[i])
  }
  const media = (sum / notas.length).toFixed(2);
  result.textContent = `A média das notas é: ${media}`
  errorMessage.textContent = ""
}

